SUMMARY = "Super Package"
DESCRIPTION = "The Super Package is really awesome"
HOMEPAGE = "https://gitlab.com/mrchapp/superpackage"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "git://gitlab.com/mrchapp/superpackage.git;protocol=https;branch=master;name=superpkg"
SRCREV = "72c7e96dbc2e7a3dae11abb52989d708cc7bf15f"

S = "${WORKDIR}/git"

do_install() {
    install -d ${D}/${bindir}
    install -m 0755 -t ${D}/${bindir} superpkg.sh
}

RDEPENDS_${PN} = "bash"

FILES_${PN} += "${B}/superpkg.sh"
